<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '5c5ebec8f8aa4ebf71f21581ae38ba311f73096b',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '5c5ebec8f8aa4ebf71f21581ae38ba311f73096b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sabberworm/php-css-parser' => array(
            'pretty_version' => '8.4.0',
            'version' => '8.4.0.0',
            'reference' => 'e41d2140031d533348b2192a83f02d8dd8a71d30',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sabberworm/php-css-parser',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
