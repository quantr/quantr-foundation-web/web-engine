<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitaabd8b81e2b5e23800fa7d1cff4b9328
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Sabberworm\\CSS\\' => 15,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Sabberworm\\CSS\\' => 
        array (
            0 => __DIR__ . '/..' . '/sabberworm/php-css-parser/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitaabd8b81e2b5e23800fa7d1cff4b9328::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitaabd8b81e2b5e23800fa7d1cff4b9328::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitaabd8b81e2b5e23800fa7d1cff4b9328::$classMap;

        }, null, ClassLoader::class);
    }
}
