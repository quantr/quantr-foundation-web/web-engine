<?php
include('WE_Theme.php');

class WebEngine
{
	public $themes = array();

	function __construct()
	{
	}

	function start()
	{
		$this->loadTheme();
		$theme = WebEngine::findTheme("Default");
		if ($theme == null) {
			die;
		}
		var_dump($theme);
	}

	function loadTheme()
	{
		$files = scandir('plugin/theme');
		foreach ($files as $file) {
			if ($file == "." || $file == "..") {
				continue;
			}
			$css = 'plugin/theme/' . $file . "/style.css";
			$cssContent = file_get_contents($css);
			$parser = new \Sabberworm\CSS\Parser($cssContent);
			$cssDocument = $parser->parse();
			$theme = new WE_Theme();
			array_push($this->themes, $theme);
			foreach ($cssDocument->getAllDeclarationBlocks() as $block) {
				foreach ($block->getComments() as $comment) {
					$lines = explode("\n", $comment);
					foreach ($lines as $line) {
						if (strpos($line, ":") !== false) {
							$temp = explode(":", $line);
							$theme->properties[trim($temp[0])] = trim($temp[1]);
						}
					}
				}
			}
		}
	}

	function findTheme($themeName)
	{
		foreach ($this->themes as $theme) {
			if ($themeName == $theme->properties['Theme Name']) {
				return $theme;
			}
		}
	}
}
